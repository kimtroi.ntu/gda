const WOW = require('wowjs');
require('slick-carousel');
import lottie from 'lottie-web';

let appFunctions = (function ($, window, undefined) {
    'use strict';
    let $win = $(window);  

    /*-----------------------------------------------------*/
    /*------------------------  init function  --------------------*/
    /*-----------------------------------------------------*/

    function _initFunction() {
        _language();
        _main_menu();
        _navBarState();
        _sliderMobile();
        _clickMenu();
        _scroll();
        _wowJs();
        _animationPayment();
        _openVideo();
    }

    function _openVideo() {
        $('.btn-watch-now').on('click', function(ev) {
            ev.preventDefault();
            $('.video-background').css({'z-index': 999, 'opacity': 1});
            $("#video")[0].src += "&autoplay=1";
            return false;
        });
    }

    /*-----------------------------------------------------*/
    /*------------------------  _language function  --------------------*/
    /*-----------------------------------------------------*/

    function _animationPayment() {
        let animation = lottie.loadAnimation({
            container: document.getElementById('payment-img'),
            renderer: 'svg',
            loop: true,
            autoplay: true,
            path: '/payment.json'
        })
    }

    /*-----------------------------------------------------*/
    /*------------------------  _language function  --------------------*/
    /*-----------------------------------------------------*/

    function _language() {
        const $menu = $('#lg-toggler');

        $(document).mouseup(e => {
           if (!$menu.is(e.target) // if the target of the click isn't the container...
           && $menu.has(e.target).length === 0) // ... nor a descendant of the container
           {
             $(".under").removeClass('is_active');
          }
         });

        $("#lg-toggler").on('click',()=>{
            $(".under").toggleClass('is_active');
            return false;
        });
    }

    /*-----------------------------------------------------*/
    /*------------------------  _language function  --------------------*/
    /*-----------------------------------------------------*/

    function _main_menu() {
        $("#main-menu").on('click',function(){
            $(".navbar").toggleClass('active');
        });
    }

    /*-----------------------------------------------------*/
    /*------------------------  _navBarState function  --------------------*/
    /*-----------------------------------------------------*/

    function _navBarState() {
        let lastScrollTop = 0;
        $(window).scroll(function(){

            let $this = $(this),
                st = $this.scrollTop(),
                navbar = $('#header');

            if ( st > 100 ) {
                navbar.addClass('scrolled');
            } else {
                navbar.removeClass('scrolled awake');
            }

            if (navbar.hasClass('scrolled') && st > 110 ) {
                // if (st > lastScrollTop){
                //     navbar.removeClass('awake');
                //     navbar.addClass('sleep');
                // } else {
                //     navbar.addClass('awake');
                //     navbar.removeClass('sleep');
                // }
                // lastScrollTop = st;

                navbar.addClass('awake');
            }

        });

        // $('#header')
        //     .mouseenter(function() {
        //         let $this = $(this);
        //         $this.addClass('awake');
        //         $this.removeClass('sleep');
        //     })
        //     .mouseleave(function() {
        //         let $this = $(this);
        //         $this.addClass('sleep');
        //         $this.removeClass('awake');
        //     });
    }

    /*-----------------------------------------------------*/
    /*------------------------  _carousel  --------------------*/
    /*-----------------------------------------------------*/

    function _sliderMobile() {
        let viewportWidth = $(window).width();

        if (viewportWidth < 768) {
            $('.sliderCenter').slick({
                centerMode: true,
                dots: true,
                arrows: false,
                slidesToShow: 1,
                mobileFirst: true,
                autoplay: true,
                autoplaySpeed: 3000,
            });

            $('.sliderCenter_2').slick({
                centerMode: true,
                dots: true,
                arrows: false,
                slidesToShow: 1,
                mobileFirst: true,
                autoplay: true,
                autoplaySpeed: 5000,
            });
        }

        $(window).on('resize', function() {
            viewportWidth = $(window).width();

            if (viewportWidth < 768) {
                $('.sliderCenter').not('.slick-initialized').slick({
                    centerMode: true,
                    dots: true,
                    arrows: false,
                    slidesToShow: 1,
                    mobileFirst: true,
                    autoplay: true,
                    autoplaySpeed: 3000,
                });

                $('.sliderCenter_2').not('.slick-initialized').slick({
                    centerMode: true,
                    dots: true,
                    arrows: false,
                    slidesToShow: 1,
                    mobileFirst: true,
                    autoplay: true,
                    autoplaySpeed: 5000,
                });
            } else {
                $('.sliderCenter.slick-initialized').slick('unslick');
                $('.sliderCenter_2.slick-initialized').slick('unslick');
            }
        });
    }

    /*-----------------------------------------------------*/
    /*------------------------  _clickMenu function  --------------------*/
    /*-----------------------------------------------------*/

    function _clickMenu() {
        let data_id;
        let scroll_data_id;
        //khi click vào item nào thì addClass 'active' và scroll đến section đó với time =500
        $(".navbar-nav li").click(function(){
            $(this).siblings().removeClass('active');
            $(this).addClass('active');
            data_id = $(this).attr('data-id');
            scroll_data_id = $("#"+data_id+"").offset().top;
            $("html, body").animate({scrollTop:scroll_data_id+1}, 500, 'swing', function() {
            });
        });
    }

    /*-----------------------------------------------------*/
    /*------------------------  _scroll function  --------------------*/
    /*-----------------------------------------------------*/

    function _scroll() {
        let offset;
        let offset_id;
        let scroll;
        $(window).scroll(function(){
            scroll = $(window).scrollTop();
            $("#main-content section").each(function(){
                offset = $(this).offset().top;
                offset_id = $(this).attr("id");

                if(scroll >= offset) {
                    $(".navbar-nav li").siblings().removeClass('active');
                    $(".navbar-nav li[data-id="+offset_id+"").addClass('active');
                }
            });
        });
    }

    /*-----------------------------------------------------*/
    /*------------------------  _wowJs function  --------------------*/
    /*-----------------------------------------------------*/

    function _wowJs() {
        window.wow = new WOW.WOW({
                boxClass:     'wow',      // animated element css class (default is wow)
                animateClass: 'animated', // animation css class (default is animated)
                offset:       0,          // distance to the element when triggering the animation (default is 0)
                mobile:       true,       // trigger animations on mobile devices (default is true)
                live:         true,       // act on asynchronously loaded content (default is true)
                callback:     function(box) {
                    // the callback is fired every time an animation is started
                    // the argument that is passed in is the DOM node being animated
                },
                scrollContainer: null // optional scroll container selector, otherwise use window
        });
        window.wow.init();
    }

    /*-----------------------------------------------------*/
    /*------------------------  export function  ---------------------*/
    /*-----------------------------------------------------*/

    function _exportFunction() {
        console.log('export function');
    }


    return {
        init: function () {
            _initFunction();
        },
        exportFunction: _exportFunction,
    };

}(jQuery, window));

jQuery(document).ready(function () {
    appFunctions.init();
});