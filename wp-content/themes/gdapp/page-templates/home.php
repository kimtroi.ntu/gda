<?php
/**
 * Template Name: Home Page
 */
get_header();
?>
    <!--Start Pull HTML here-->
    <main id="main-content" class="main">
        <?php get_template_part('template-parts/blocks/home/banner') ?>
        <?php get_template_part('template-parts/blocks/home/gpay') ?>
        <?php get_template_part('template-parts/blocks/home/process') ?>
        <?php get_template_part('template-parts/blocks/home/features') ?>
        <?php get_template_part('template-parts/blocks/home/payment') ?>
    </main>
    <!--END  Pull HTML here-->
<?php get_footer();

