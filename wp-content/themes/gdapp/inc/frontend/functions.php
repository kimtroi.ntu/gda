<?php if (! defined('APP_PATH')) die ('Bad requested!');

//adding a body class to a specific page template
add_filter( 'body_class', 'custom_class' );
function custom_class( $classes ) {
    $classes[] = 'gdapp';
    return $classes;
}

// translate-custom
function wpedu_translate(){
    /**
     * Key = Name
     * Value = String
     */
    $arr = array(
        'contact us'  => 'contact-us',
        'Email with us:' => 'email-with-us',
        'connect with us' => 'connect-with-us',
        'Footer title' 	=> 'footer-title',
        'Footer description' => 'footer-des',
        'About Us' => 'about-us',
        'Product' => 'product',
        'Home' => 'menu-home',
        'GPAY' => 'menu-gpay',
        'PROCESS' => 'menu-process',
        'FEATURES' => 'menu-features',
        'PAYMENT SUPPORT' => 'menu-payment-support',
        'EN' => 'menu-en',
        'ZH' => 'menu-zh',
    );

    foreach( $arr as $key => $value ) {
        pll_register_string($key, $value, 'translate-custom');
    }
}
add_action( 'after_setup_theme', 'wpedu_translate' );
