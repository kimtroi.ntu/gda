<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?= get_bloginfo('charset'); ?>">
    <meta name="viewport" content="initial-scale=1.0,width=device-width,height=device-height,shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="google" content="notranslate">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <link rel="apple-touch-icon" sizes="57x57" href="<?= ASSETS_PATH ?>images/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= ASSETS_PATH ?>images/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= ASSETS_PATH ?>images/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= ASSETS_PATH ?>images/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= ASSETS_PATH ?>images/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= ASSETS_PATH ?>images/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= ASSETS_PATH ?>images/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= ASSETS_PATH ?>images/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= ASSETS_PATH ?>images/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"
          href="<?= ASSETS_PATH ?>images/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= ASSETS_PATH ?>images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= ASSETS_PATH ?>images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= ASSETS_PATH ?>images/favicon-16x16.png">
    <link rel="manifest" href="<?= ASSETS_PATH ?>images/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= ASSETS_PATH ?>images/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <?php if (is_singular() && pings_open(get_queried_object())) : ?>
        <link rel="pingback" href="<?= get_bloginfo('pingback_url'); ?>">
    <?php endif; ?>

    <link href="https://fonts.googleapis.com/css?family=Muli:400,800,700&display=swap" rel="stylesheet">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="page-wrap">
    <header id="header" class="block-header fixed">
        <nav class="navbar navbar-expand-lg">
            <div class="container">
                <a class="navbar-brand" href="<?= home_url() ?>">
                    <img class="logo d-block d-lg-none" src="<?= wpedu_get_option('option_logo_mb')['url'] ?>" alt="<?= get_bloginfo('name') ?>">
                </a>
                <a class="navbar-brand" href="<?= home_url() ?>">
                    <img class="logo d-lg-block" src="<?= wpedu_get_option('option_logo')['url'] ?>" alt="<?= get_bloginfo('name') ?>">
                </a>

                <div class="d-flex">
                    <?php $languages = pll_the_languages(array('hide_current'=>1,'raw'=>1)); ?>
                    <?php foreach ($languages as $key => $language) : ?>
                    <div class="icon-language d-block d-lg-none mr-3 <?= implode(" ", $language['classes']); ?>">
                        <a lang="<?= $language['locale'] ?>" hreflang="<?= $language['locale'] ?>" href="<?= $language['url'] ?>">
                            <img src="<?= ASSETS_PATH ?>images/flag_<?= $language['slug'] ?>.png" alt="flag_<?= $language['slug'] ?>">
                        </a>
                    </div>
                    <?php endforeach; ?>

                    <button id="main-menu" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"><img src="<?= ASSETS_PATH ?>images/navbar.png" alt=""></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav ml-auto">
                        <li data-id="banner" class="nav-item">
                            <a class="nav-link " href="#banner" title="HOME"><?= pll__('menu-home') ?></a>
                        </li>
                        <li data-id="gpay" class="nav-item">
                            <a class="nav-link" href="#gpay" title="<?= pll__('menu-gpay') ?>"><?= pll__('menu-gpay') ?></a>
                        </li>
                        <li data-id="process" class="nav-item">
                            <a class="nav-link" href="#process" title="<?= pll__('menu-process') ?>"><?= pll__('menu-process') ?></a>
                        </li>
                        <li data-id="features" class="nav-item">
                            <a class="nav-link" href="#features" title="<?= pll__('menu-features') ?>"><?= pll__('menu-features') ?></a>
                        </li>
                        <li data-id="payment" class="nav-item">
                            <a class="nav-link" href="#payment" title="<?= pll__('menu-payment-support') ?>"><?= pll__('menu-payment-support') ?></a>
                        </li>
                    </ul>

                    <ul id="language" class="d-none d-lg-block">
                        <li id="lg-toggler" class="nav-item active">
                            <a class="nav-link d-flex align-items-center justify-content-between" href="javascript:(0)">
                                <?= pll__('menu-' . pll_current_language('slug')) ?>
                                <span class="p-0"><img src="<?= ASSETS_PATH ?>images/arrow.png" alt=""><img src="<?= ASSETS_PATH ?>images/arrow_bl.png" alt="" class="img_bl"></span>
                            </a>
                        </li>

                        <?php foreach ($languages as $key => $language) : ?>
                        <li class="nav-item under hide <?= implode(" ", $language['classes']); ?>">
                            <a class="nav-link" lang="<?= $language['locale'] ?>" hreflang="<?= $language['locale'] ?>" href="<?= $language['url'] ?>">
                                <?= pll__('menu-' . $language['slug']) ?>
                            </a>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
