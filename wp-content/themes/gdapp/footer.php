<footer id="footer">
    <div class="container">
        <img class="ft-logo img-fluid" src="<?= wpedu_get_option('option_footer_logo')['url'] ?>" alt="<?= get_bloginfo('name') ?>">

        <h3><?= pll__('footer-title') ?></h3>

        <div class="des">
            <p><?= pll__('footer-des') ?></p>
        </div>

        <div class="connect-with-us">
            <h4><?= pll__('connect-with-us') ?></h4>
            <?php get_template_part('template-parts/components/connect-with-us') ?>
        </div>

        <div class="ft-list">
            <div class="group order-lg-last">
                <h4><?= pll__('about-us') ?></h4>
                <?php
                wp_nav_menu(array(
                    'theme_location'  => 'about_us_menu',
                    'container'       => '',
                    'container_class' => '',
                    'container_id'    => '',
                    'menu_class'      => ''
                ));
                ?>
            </div>

            <div class="group">
                <h4><?= pll__('product') ?></h4>
                <?php
                wp_nav_menu(array(
                    'theme_location'  => 'product_menu',
                    'container'       => '',
                    'container_class' => '',
                    'container_id'    => '',
                    'menu_class'      => ''
                ));
                ?>
            </div>

            <div class="group">
                <h4><?= pll__('contact-us') ?></h4>
                <p><?= pll__('email-with-us') ?></p>
                <ul>
                    <li><a class="email" href="mailto:<?= wpedu_get_option('option_email') ?>"><?= wpedu_get_option('option_email') ?></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="ft-bottom d-none d-md-block">
        <div class="container d-flex justify-content-between">
            <?php
            $copyrightFooter = str_replace('%y%', date('Y'), wpedu_get_option('option_text_copyright'));
            printf('<p class="t-left text-left">%s</p>', $copyrightFooter);

            wp_nav_menu(array(
                'theme_location'  => 'footer_menu',
                'container'       => '',
                'container_class' => '',
                'container_id'    => '',
                'menu_class'      => 'd-flex t-right'
            ));
            ?>
        </div>
    </div>
</footer>
</div>
<?php wp_footer(); ?>
<?= wpedu_get_option('option_footer_code') ?>
</body>
</html>