<?php
$get_set_up = get_field('get_set_up');
$checkout = get_field('checkout');
?>
<section id="gpay">
    <div id="gpay-top">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-5">
                    <div class="box-gpay-t">
                        <h3 class="block-title"><?= $get_set_up['title'] ?></h3>

                        <div class="describle cl_cfcfff">
                            <?= $get_set_up['description'] ?>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-7">
                    <div class="row sliderCenter_2">
                        <div class="col-12 col-md-4">
                            <div class="box-step step1 wow slideInUp"  data-wow-duration="0.8s" data-wow-delay="0.2s">
                                <img class="img-fluid" src="<?= $get_set_up['step_image_1'] ?>" alt="step 1">
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="box-step step2 wow slideInUp"  data-wow-duration="0.8s" data-wow-delay="0.6s">
                                <img class="img-fluid" src="<?= $get_set_up['step_image_2'] ?>" alt="step 2">
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="box-step step3 wow slideInUp"  data-wow-duration="0.8s" data-wow-delay="1s">
                                <img class="img-fluid" src="<?= $get_set_up['step_image_3'] ?>" alt="step 3">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="gpay-bottom">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-5 order-0 order-md-1">
                    <div class="box-gpay-b">
                        <h3 class="block-title _blue"><?= $checkout['title'] ?></h3>
                        <div class="caption  describle">
                            <?= $checkout['description'] ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 order-1 order-md-0">
                    <div class="wow slideInUp"  data-wow-duration="0.8s" data-wow-delay="0.6s">
                        <img class="img-fluid" src="<?= $checkout['image'] ?>" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>