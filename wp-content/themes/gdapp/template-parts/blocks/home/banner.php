<?php
$banner = get_field('banner');
$three_boxs = get_field('three_boxs');
?>
<section id="banner">
    <div id="banner-top" class="d-flex align-items-sm-center flex-wrap">
        <div class="bn-cover">
            <div class="container">
                <img src="<?= ASSETS_PATH ?>images/frame.png" class="img-fluid" alt="">
            </div>
        </div>

        <div class="bn-info w-100">
            <div class="container">
                <div class="row align-items-sm-center">
                    <div class="col-5 col-lg-6 pr-0">
                        <div class="bn-left">
                            <h1 class="main-title">
                                <?= $banner['title'] ?>
                            </h1>
                            <div class="caption d-none d-md-block">
                                <?= $banner['description'] ?>
                            </div>
                            <a id="play-button" class="btn-watch-now d-none d-md-flex align-items-center justify-content-center" href="#">
                                <img src="<?= ASSETS_PATH ?>images/watch_now.png" class="mr-2">
                                <?= $banner['button_text'] ?>
                            </a>
                        </div>
                    </div>

                    <div class="col-7 col-lg-6 p-0">
                        <div id="payment-img" class="bn-right">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="bg-bottom-1 d-none d-sm-block">
            <div class="first" style="background-image: url('<?= ASSETS_PATH ?>images/banner-bottom-top.png')"></div>
            <div class="second" style="background-image: url('<?= ASSETS_PATH ?>images/banner-bottom-bottom.png')"></div>
        </div>

        <div class="bg-bottom-2 d-none d-sm-block" style="background-image: url('<?= ASSETS_PATH ?>images/bg_bottom.png')"></div>

        <?php if ( isset($banner['video_id']) ) : ?>
            <div class="video-background d-none d-md-block">
                <div class="video-foreground">
                    <iframe id="video" src="https://www.youtube-nocookie.com/embed/<?= $banner['video_id'] ?>?controls=0&showinfo=0&rel=0&autohide=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        <?php endif; ?>
    </div>

    <div id="banner-bottom">
        <div class="container">
            <div class="row sliderCenter" >
                <?php for ($i=1; $i<4; $i++) : ?>
                <div class="col-md-4">
                    <div class="box">
                        <div class="bg">
                            <div class="bg-img">
                                <?= $three_boxs['box_'.$i]['icon'] ?>
                            </div>
                        </div>

                        <h4 class="box-title"><?= $three_boxs['box_'.$i]['title'] ?></h4>
                        <div class="describle  cl_757575">
                            <p><?= $three_boxs['box_'.$i]['description'] ?></p>
                        </div>
                    </div>
                </div>
                <?php endfor; ?>
            </div>
        </div>
    </div>
</section>