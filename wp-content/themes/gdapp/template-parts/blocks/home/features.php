<?php
$features = get_field('features');
$boxes = $features['six_boxs'];
?>
<section id="features">
    <div class="container">
        <div class="features-top">
            <h3 class="block-title _blue title"><?= $features['title'] ?></h3>
        </div>
    </div>

    <div class="feature-bottom d-flex">
        <div class="container group">
            <div class="row">
                <?php for ($i=1; $i<7; $i++) : ?>
                <div class="col-12 col-md-6 col-xl-4">
                    <div class="box">
                        <a href="#" class="d-flex">
                            <div class="img align-self-center">
                                <object class="img-fluid" type="image/svg+xml" data="<?= $boxes['box_'.$i]['icon'] ?>">
                                    <!-- Your fall back here -->
                                    <img class="img-fluid" src="<?= $boxes['box_'.$i]['icon'] ?>" />
                                </object>
                            </div>

                            <div class="content">
                                <h4 class="box-title"><?= $boxes['box_'.$i]['title'] ?></h4>

                                <div class="describle cl_757575">
                                    <p><?= $boxes['box_'.$i]['description'] ?></p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <?php endfor; ?>
            </div>
        </div>
    </div>
</section>