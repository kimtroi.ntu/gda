<?php
$process = get_field('process');
$boxes = $process['five_boxs'];
?>
<section id="process">
    <div class="bg_process">
        <div class="container">
            <div class="title">
                <h3 class="block-title"><?= $process['title'] ?></h3>
                <?= $process['description'] ?>
            </div>
        </div>
    </div>

    <div class="bg-group">
        <div class="container group">
            <div class="box wow zoomIn" data-wow-duration="0.8s" data-wow-delay="0.2s">
                <object class="img-fluid" type="image/svg+xml" data="<?= $boxes['box_1']['icon'] ?>">
                    <!-- Your fall back here -->
                    <img class="img-fluid" src="<?= $boxes['box_1']['icon'] ?>" />
                </object>
                <p><?= $boxes['box_1']['description'] ?></p>
            </div>

            <div class="box wow zoomIn" data-wow-duration="0.8s" data-wow-delay="0.6s">
                <object class="img-fluid" type="image/svg+xml" data="<?= $boxes['box_2']['icon'] ?>">
                    <!-- Your fall back here -->
                    <img class="img-fluid" src="<?= $boxes['box_2']['icon'] ?>" />
                </object>
                <p><?= $boxes['box_2']['description'] ?></p>
            </div>


            <div class="box wow zoomIn" data-wow-duration="0.8s" data-wow-delay="1s">
                <object class="img-fluid" type="image/svg+xml" data="<?= $boxes['box_3']['icon'] ?>">
                    <!-- Your fall back here -->
                    <img class="img-fluid" src="<?= $boxes['box_3']['icon'] ?>" />
                </object>
                <p><?= $boxes['box_3']['description'] ?></p>
            </div>

            <div class="box wow zoomIn" data-wow-duration="0.8s" data-wow-delay="1.4s">
                <object class="img-fluid" type="image/svg+xml" data="<?= $boxes['box_4']['icon'] ?>">
                    <!-- Your fall back here -->
                    <img class="img-fluid" src="<?= $boxes['box_4']['icon'] ?>" />
                </object>
                <p><?= $boxes['box_4']['description'] ?></p>
            </div>

            <div class="box wow zoomIn" data-wow-duration="0.8s" data-wow-delay="1.8s">
                <object class="img-fluid" type="image/svg+xml" data="<?= $boxes['box_5']['icon'] ?>">
                    <!-- Your fall back here -->
                    <img class="img-fluid" src="<?= $boxes['box_5']['icon'] ?>" />
                </object>
                <p><?= $boxes['box_5']['description'] ?></p>
            </div>
        </div>
    </div>

    <div class="bg_process_bottom">
        
    </div>

</section>