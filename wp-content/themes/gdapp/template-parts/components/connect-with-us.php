<?php
$social = array();
$social['facebook'] = wpedu_get_option('social_facebook');
$social['telegram'] = wpedu_get_option('social_telegram');
$social['youtube'] = wpedu_get_option('social_youtube');
$social['medium'] = wpedu_get_option('social_medium');
?>
<div class="btn-connect">
    <ul>
        <?php
        foreach ($social as $key => $value) {
            if ( $value )
                printf('<li><a class="%1$s" href="%2$s" target="_blank"><span class="pr-0 pr-md-2"><img src="%3$simages/%1$s.png" alt="%1$s"></span><span class="d-none d-md-inline-block">%1$s</span></a></li>', $key, $value, ASSETS_PATH);
        }
        ?>
    </ul>
</div>