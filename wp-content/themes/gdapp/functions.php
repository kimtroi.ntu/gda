<?php
define('APP_PATH', dirname(__FILE__));
/**
 * CORE Includes
 **/
include APP_PATH.'/inc/define.php';
include APP_PATH.'/inc/setup.php';
include APP_PATH.'/inc/admin/functions.php';
require_once APP_PATH.'/ReduxCore/framework.php';
require_once APP_PATH.'/option.php';
include APP_PATH.'/inc/plugins/acf/functions.php';
include APP_PATH.'/inc/frontend/functions.php';